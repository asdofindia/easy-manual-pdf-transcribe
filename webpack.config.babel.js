import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'

export default {
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9002
    },
    entry: {
        'main': './src/js/main.js',
        'pdf.worker': 'pdfjs-dist/build/pdf.worker.entry'
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                },
            },
            {
                test: /.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
            excludeChunks: ['pdf.worker']
        })
    ]
}
