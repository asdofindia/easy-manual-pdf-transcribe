import React, { useState } from 'react'
import 'handsontable/dist/handsontable.full.css'
import { HotTable } from '@handsontable/react'

const Button = ({onClick}) => {
    return <button onClick={onClick}>Save</button>
}

export const Welcome = () => {
    const hotTableRef = React.createRef()

    const triggerSave = () => {
        hotTableRef.current.hotInstance.getPlugin('exportFile').downloadFile('csv', {filename: 'table'})
    }
    return <div>
        <Button onClick={triggerSave}/>
        <HotTable licenseKey="non-commercial-and-evaluation" ref={hotTableRef} />
    </div>
}

