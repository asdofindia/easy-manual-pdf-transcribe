import { init as initPDF } from './pdf/api'

import React from 'react'
import ReactDOM from 'react-dom'
import { Welcome } from './App'

import '../css/main.css'

initPDF()

ReactDOM.render(<Welcome />, document.querySelector("#react-root"))
