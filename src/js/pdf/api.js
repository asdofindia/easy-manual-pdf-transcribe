import PDFJS from 'pdfjs-dist/webpack'
PDFJS.GlobalWorkerOptions.workerSrc = './pdf.worker.bundle.js'

export const loadURL = async (url) => {
    const loadingTask = PDFJS.getDocument(url);
    const pdf = await loadingTask.promise.catch(console.error);
    console.log('PDF loaded');

    const pageNumber = 1;
    const page = await pdf.getPage(pageNumber).catch(console.error);

    console.log('Page loaded');

    const canvas = document.getElementById('the-canvas');
    const context = canvas.getContext('2d');

    const desiredWidth = 700;
    const viewport = page.getViewport({ scale: 1, });
    const scale = desiredWidth / viewport.width;
    const scaledViewport = page.getViewport({ scale: scale, });

    canvas.width=desiredWidth;
    canvas.height = scaledViewport.height;

    const renderContext = {
        canvasContext: context,
        viewport: scaledViewport
    };

    const renderTask = page.render(renderContext);
    await renderTask.promise;
    console.log('Page rendered');
};

export const getURLFromForm = (form) => {
    return form.elements[0].value;
}

export const init = () => {
    const urlForm = document.querySelector('#url-form');
    const loadButton = document.querySelector('#loadButton')
    loadButton.addEventListener('click', (e) => { e.preventDefault(); loadURL(getURLFromForm(urlForm))});
}
